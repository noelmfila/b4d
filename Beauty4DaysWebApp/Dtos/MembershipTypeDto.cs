﻿namespace Beauty4DaysWebApp.Dtos
{
    public class MembershipTypeDto
    {
        public int Id { get; set; }

        public string MembershipName { get; set; }

        public byte DiscountRate { get; set; }
    }
}