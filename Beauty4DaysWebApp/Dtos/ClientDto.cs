﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Beauty4DaysWebApp.Dtos
{
    public class ClientDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter your full name(s)!")]
        [StringLength(100)]
        public string ClientName { get; set; }

        public bool IsSubscribed { get; set; }

        [Required(ErrorMessage = "Please choose your preferred membership type!")]
        public int MembershipTypeId { get; set; }

        public MembershipTypeDto MembershipType { get; set; }

        //[Min18ForMembership]
        public DateTime? BirthDate { get; set; }
    }
}