﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Beauty4DaysWebApp.Models
{
    public class Client
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter your full name(s)!")]
        [Display(Name = "Full Name")]
        [StringLength(100)]
        public string ClientName { get; set; }

        [Display(Name = "Subscribe to promotional notifications?")]
        public bool IsSubscribed { get; set; }

        [Required(ErrorMessage = "Please choose your preferred membership type!")]
        [Display(Name = "Membership Type")]
        public int MembershipTypeId { get; set; }

        public MembershipType MembershipType { get; set; }

        [Display(Name = "Date of Birth")]
        [Min18ForMembership]
        public DateTime? BirthDate { get; set; }

    }
}

