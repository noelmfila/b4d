﻿namespace Beauty4DaysWebApp.Models
{
    public class Manicure
    {
        public int Id { get; set; }
        public string ManicureName { get; set; }
    }
}