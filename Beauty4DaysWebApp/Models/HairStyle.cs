﻿namespace Beauty4DaysWebApp.Models
{
    public class HairStyle
    {
        public int Id { get; set; }
        public string HairStyleName { get; set; }
    }
}