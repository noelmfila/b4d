﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Beauty4DaysWebApp.Models
{
    public class Min18ForMembership : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var client = (Client)validationContext.ObjectInstance;
            if (client.MembershipTypeId == MembershipType.UnKnown ||
                client.MembershipTypeId == MembershipType.PayAsYouGo)
                return ValidationResult.Success;

            if (client.BirthDate == null)
                return new ValidationResult("Please specify your birth date for any membership!");

            //Crude present age calculation
            var age = DateTime.Today.Year - client.BirthDate.Value.Year;

            return (age >= 18)
                ? ValidationResult.Success
                : new ValidationResult("You must be at least 18yrs old to subscribe to any membership!");
        }

    }
}