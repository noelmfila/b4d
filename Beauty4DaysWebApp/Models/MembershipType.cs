﻿namespace Beauty4DaysWebApp.Models
{
    public class MembershipType
    {
        public int Id { get; set; }
        public string MembershipName { get; set; }
        public short SignUpFee { get; set; }
        public byte DurationInMonths { get; set; }
        public byte DiscountRate { get; set; }

        public static readonly int UnKnown = 0;
        public static readonly int PayAsYouGo = 1;
    }
}

