﻿namespace Beauty4DaysWebApp.Models
{
    public class Pedicure
    {
        public int Id { get; set; }
        public string PedicureName { get; set; }
    }
}