﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Beauty4DaysWebApp.Startup))]
namespace Beauty4DaysWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
