﻿using AutoMapper;
using Beauty4DaysWebApp.Dtos;
using Beauty4DaysWebApp.Models;


namespace Beauty4DaysWebApp.App_Start
{
    public class MappingProfile
    {
        public static void RegisterMaps()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Client, ClientDto>();
                config.CreateMap<MembershipType, MembershipTypeDto>();
                //.ForMember(c => c.Id, opt => opt.Ignore());

                config.CreateMap<ClientDto, Client>()
                    .ForMember(c => c.Id, opt => opt.Ignore());
            });
        }
    }

}