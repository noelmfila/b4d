﻿using AutoMapper;
using Beauty4DaysWebApp.Dtos;
using Beauty4DaysWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Http;


namespace Beauty4DaysWebApp.Controllers.Api
{
    public class ClientsController : ApiController
    {
        private readonly ApplicationDbContext _context;

        public ClientsController()
        {
            _context = new ApplicationDbContext();

        }

        // GET: api/Clients
        [HttpGet]
        public IEnumerable<ClientDto> Clients()
        {
            return _context.Clients.Include(c => c.MembershipType).ToList().Select(Mapper.Map<Client, ClientDto>);
        }

        // GET: api/Clients/5
        [HttpGet]
        public IHttpActionResult Client(int id)
        {
            var client = _context.Clients.SingleOrDefault(c => c.Id == id);

            if (client == null)
                return NotFound();

            return Ok((Mapper.Map<Client, ClientDto>(client)));
        }

        // POST: api/Clients
        [HttpPost]
        public IHttpActionResult CreateClient(ClientDto clientDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var client = Mapper.Map<ClientDto, Client>(clientDto);
            _context.Clients.Add(client);
            _context.SaveChanges();

            //Generated Id should be passed to the DTO 
            clientDto.Id = client.Id;

            return Created(new Uri(Request.RequestUri + "/" + clientDto.Id), clientDto);
        }


        // PUT: api/Clients/5
        [HttpPut]
        public void UpdateClient(int id, ClientDto clientDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var clientInDb = _context.Clients.SingleOrDefault(c => c.Id == id);

            if (clientInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map(clientDto, clientInDb);
            _context.SaveChanges();

        }

        // DELETE: api/Clients/5
        [HttpDelete]
        public void TestClients(int id)
        {
            var client = _context.Clients.SingleOrDefault(c => c.Id == id);

            if (client == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.Clients.Remove(client);
            _context.SaveChanges();

        }

        private bool ClientExists(int id)
        {
            return _context.Clients.Count(e => e.Id == id) > 0;
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

    }
}