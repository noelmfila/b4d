﻿using Beauty4DaysWebApp.Models;
using Beauty4DaysWebApp.ViewModel;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Beauty4DaysWebApp.Controllers
{
    public class ClientController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ClientController()
        {
            _context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            var clientViewModel = new ClientViewModel
            {
                Client = new Client(),
                MembershipTypes = _context.MembershipTypes.ToList()
            };
            return View(clientViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(ClientViewModel clientViewModel)
        {

            if (!ModelState.IsValid)
            {
                var sameViewModel = new ClientViewModel
                {
                    Client = clientViewModel.Client,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };
                return View("Create", sameViewModel);
            }

            if (clientViewModel.Client.Id == 0)
                _context.Clients.Add(clientViewModel.Client);
            else
            {
                var clientInDb = _context.Clients.Single(c => c.Id == clientViewModel.Client.Id);

                clientInDb.ClientName = clientViewModel.Client.ClientName;
                clientInDb.BirthDate = clientViewModel.Client.BirthDate;
                clientInDb.MembershipTypeId = clientViewModel.Client.MembershipTypeId;
                clientInDb.IsSubscribed = clientViewModel.Client.IsSubscribed;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Client");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var client = _context.Clients.SingleOrDefault(c => c.Id == id);

            if (client == null)
                return HttpNotFound();

            var clientViewModel = new ClientViewModel
            {
                Client = client,
                MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("Create", clientViewModel);
        }

        public ActionResult Details(int id)
        {
            var client = _context.Clients.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

            if (client == null)
                return HttpNotFound();

            return View(client);
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
    }
}