﻿using Beauty4DaysWebApp.Models;
using System.Collections.Generic;

namespace Beauty4DaysWebApp.ViewModel
{
    public class ClientViewModel
    {
        public Client Client { get; set; }

        public IEnumerable<MembershipType> MembershipTypes { get; set; }

        public Manicure Manicure { get; set; }
        public Pedicure Pedicure { get; set; }
        public HairStyle HairStyle { get; set; }

        public string Title {
            get
            {
                return (Client.Id != 0)? "Edit Profile" : "Create Profile";

            }
        }


    }
}