namespace Beauty4DaysWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'6ce15641-bbe6-4dad-90dc-da643b5f4a97', N'admin@b4d.org', 0, N'AKsJGs1MUVO6+6eE/jeMHdmfgRFW/8oECOFUzY2oEWTD+UuVzBLZpWKF8zJgoVO58Q==', N'efcbb429-1e4b-4ebe-a9fd-9300ecd9b2e4', NULL, 0, 0, NULL, 1, 0, N'admin@b4d.org')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'85d87b3c-366d-465e-abe3-b33098fb6513', N'guest@b4d.org', 0, N'APEEWRMxDk7CrjZLoi/rO5Is1An9iYgfspkndDPIhGJLaxtKB8y/Sj7/LQQyO+4I4Q==', N'461d89cc-1883-4f41-b365-69e5d1094877', NULL, 0, 0, NULL, 1, 0, N'guest@b4d.org')
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'fff6f27f-4fbb-40b3-87f5-13ec922df18b', N'CanManageClients')
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'6ce15641-bbe6-4dad-90dc-da643b5f4a97', N'fff6f27f-4fbb-40b3-87f5-13ec922df18b')

                ");
        }
        
        public override void Down()
        {
        }
    }
}
