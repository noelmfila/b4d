namespace Beauty4DaysWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddManicuresTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Manicures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ManicureName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Manicures");
        }
    }
}
