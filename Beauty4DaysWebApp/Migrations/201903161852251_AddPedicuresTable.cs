namespace Beauty4DaysWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPedicuresTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pedicures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PedicureName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pedicures");
        }
    }
}
