namespace Beauty4DaysWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeyToClientTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Clients", "MembershipTypeId_Id", "dbo.MembershipTypes");
            DropForeignKey("dbo.Clients", "MembershipType_Id", "dbo.MembershipTypes");
            DropIndex("dbo.Clients", new[] { "MembershipType_Id" });
            DropIndex("dbo.Clients", new[] { "MembershipTypeId_Id" });
            RenameColumn(table: "dbo.Clients", name: "MembershipType_Id", newName: "MembershipTypeId");
            AlterColumn("dbo.Clients", "MembershipTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Clients", "MembershipTypeId");
            AddForeignKey("dbo.Clients", "MembershipTypeId", "dbo.MembershipTypes", "Id", cascadeDelete: true);
            DropColumn("dbo.Clients", "MembershipTypeId_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clients", "MembershipTypeId_Id", c => c.Int());
            DropForeignKey("dbo.Clients", "MembershipTypeId", "dbo.MembershipTypes");
            DropIndex("dbo.Clients", new[] { "MembershipTypeId" });
            AlterColumn("dbo.Clients", "MembershipTypeId", c => c.Int());
            RenameColumn(table: "dbo.Clients", name: "MembershipTypeId", newName: "MembershipType_Id");
            CreateIndex("dbo.Clients", "MembershipTypeId_Id");
            CreateIndex("dbo.Clients", "MembershipType_Id");
            AddForeignKey("dbo.Clients", "MembershipType_Id", "dbo.MembershipTypes", "Id");
            AddForeignKey("dbo.Clients", "MembershipTypeId_Id", "dbo.MembershipTypes", "Id");
        }
    }
}
