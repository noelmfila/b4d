namespace Beauty4DaysWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMembershipTypeTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MembershipTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SignUpFee = c.Short(nullable: false),
                        DurationInMonths = c.Byte(nullable: false),
                        DiscountRate = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Clients", "IsSubscribed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Clients", "MembershipType_Id", c => c.Int());
            AddColumn("dbo.Clients", "MembershipTypeId_Id", c => c.Int());
            CreateIndex("dbo.Clients", "MembershipType_Id");
            CreateIndex("dbo.Clients", "MembershipTypeId_Id");
            AddForeignKey("dbo.Clients", "MembershipType_Id", "dbo.MembershipTypes", "Id");
            AddForeignKey("dbo.Clients", "MembershipTypeId_Id", "dbo.MembershipTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Clients", "MembershipTypeId_Id", "dbo.MembershipTypes");
            DropForeignKey("dbo.Clients", "MembershipType_Id", "dbo.MembershipTypes");
            DropIndex("dbo.Clients", new[] { "MembershipTypeId_Id" });
            DropIndex("dbo.Clients", new[] { "MembershipType_Id" });
            DropColumn("dbo.Clients", "MembershipTypeId_Id");
            DropColumn("dbo.Clients", "MembershipType_Id");
            DropColumn("dbo.Clients", "IsSubscribed");
            DropTable("dbo.MembershipTypes");
        }
    }
}
