namespace Beauty4DaysWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMembershipTypes : DbMigration
    {
        public override void Up()
        {
            Sql("Insert Into MembershipTypes ( SignUpFee, DurationInMonths, DiscountRate) Values ( 0, 0, 0)");
            Sql("Insert Into MembershipTypes ( SignUpFee, DurationInMonths, DiscountRate) Values ( 50, 1, 5)");
            Sql("Insert Into MembershipTypes ( SignUpFee, DurationInMonths, DiscountRate) Values ( 100, 3, 10)");
            Sql("Insert Into MembershipTypes ( SignUpFee, DurationInMonths, DiscountRate) Values ( 200, 6, 20)");
            Sql("Insert Into MembershipTypes ( SignUpFee, DurationInMonths, DiscountRate) Values ( 300, 12, 30)");
        }
        
        public override void Down()
        {
        }
    }
}
