namespace Beauty4DaysWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHairStylesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HairStyles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HairStyleName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.HairStyles");
        }
    }
}
